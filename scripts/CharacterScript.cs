﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CharacterScript : MonoBehaviour
{
    public Transform target;
    public Transform head;
    public Transform body;


    bool horiz = false;
    bool vert = false;

    public float speed = 6.0f;
    public float jumpSpeed = 8.0f;
    public float gravity = 20.0f;

    int jumpHash = Animator.StringToHash("Running");

    private Vector3 moveDirection = Vector3.zero; // = new Vector3(0,0,0);
    private CharacterController controller;

    // Start is called before the first frame update
    void Start()
    {
        controller = GetComponent<CharacterController>();

        // let the gameObject fall down
        //starting position
        //gameObject.transform.position = new Vector3(0, 10, 0);

    }

    // Update is called once per frame
    void Update()
    {
        handleInput();
        handleRotation();

        if (controller.isGrounded)
        {
            // We are grounded, so recalculate
            // move direction directly from axes

            moveDirection = new Vector3(Input.GetAxis("Horizontal"), 0.0f, Input.GetAxis("Vertical"));
            moveDirection = transform.TransformDirection(moveDirection);
            moveDirection = moveDirection * speed;

            if (Input.GetButton("Jump"))
            {
                moveDirection.y = jumpSpeed;
            }
        }

        // Apply gravity
        moveDirection.y = moveDirection.y - (gravity * Time.deltaTime);

        // Move the controller
        controller.Move(moveDirection * Time.deltaTime);

        loadNextScene();

        // oculus remote

        Vector2 tPos = OVRInput.Get(OVRInput.Axis2D.PrimaryTouchpad, OVRInput.Controller.RTrackedRemote);

        float x = tPos.x;
        float z = tPos.y;

        transform.Translate(0, 0, z / 5);
        transform.Translate(x / 5, 0, 0);
        
    }

    void handleRotation()
    {
        Vector3 HeadRotation = head.transform.rotation.eulerAngles;

        Vector3 euler = new Vector3(0, HeadRotation.y, 0);

        //transform.eulerAngles = Vector3.Lerp(transform.rotation.eulerAngles, HeadRotation, 5 * Time.deltaTime);
        body.eulerAngles = Vector3.Lerp(body.rotation.eulerAngles, euler, 5 * Time.deltaTime);


    }
    

    void handleInput()
    {
        //oculus
        Vector2 touchPos = OVRInput.Get(OVRInput.Axis2D.PrimaryTouchpad, OVRInput.Controller.RTrackedRemote);

    }

    public void loadNextScene()
    {

        if (transform.position.z > target.position.z)
        {
            SceneManager.LoadScene(1);
        }
    }

}
