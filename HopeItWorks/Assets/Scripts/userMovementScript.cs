﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class userMovementScript : MonoBehaviour
{
    bool horiz = false;
    bool vert = false;

    public Transform goalPos;

    //this is hopefully the 2d vector for touchpad
    public Vector2 inputPad = OVRInput.Get(OVRInput.Axis2D.PrimaryTouchpad, OVRInput.Controller.RTrackedRemote);

    // Start is called before the first frame update
    void Start()
    {
        print("capsule is started!");
        print(Input.GetAxis("Horizontal"));
    }

    // Update is called once per frame
    void Update()
    {
        handleInput();

        handleMovement();

        loadNextScene();

        //rotation of player 


        //transform.LookAt(target);

    }
    void handleInput()
    {
        //keyboard
        if (Input.GetButtonDown("Horizontal"))
        {
            horiz = !horiz;
        }

        if (Input.GetButtonDown("Vertical"))
        {
            vert = !vert;
        }

        //oculus
        Vector2 touchPos = OVRInput.Get(OVRInput.Axis2D.PrimaryTouchpad, OVRInput.Controller.RTrackedRemote);

    }

    public void loadNextScene() {

        if (transform.position.z > goalPos.position.z)
        {
            SceneManager.LoadScene(1);
        }
    }
    void handleMovement()
    {

        //keyboard
        if (horiz)
        {
            transform.Translate((Input.GetAxis("Horizontal") * 5) * Time.deltaTime, 0, 0);

        }
        if (vert)
        {
            transform.Translate(0, 0, (Input.GetAxis("Vertical") * 5) * Time.deltaTime);

        }

        // oculus remote

        Vector2 tPos = OVRInput.Get(OVRInput.Axis2D.PrimaryTouchpad, OVRInput.Controller.RTrackedRemote);

        float x = tPos.x;
        float z = tPos.y;

        transform.Translate(0, 0, z / 10);
        transform.Translate(x / 10, 0, 0);
    }
}


