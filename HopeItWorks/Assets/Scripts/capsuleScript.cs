﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class capsuleScript : MonoBehaviour
{
    bool horiz = false;
    bool vert = false;


    //this is hopefully the 2d vector for touchpad
    public Vector2 inputPad = OVRInput.Get(OVRInput.Axis2D.PrimaryTouchpad, OVRInput.Controller.RTrackedRemote);

    // Start is called before the first frame update
    void Start()
    {
        print("capsule is started!");
        print(Input.GetAxis("Horizontal"));
    }

    // Update is called once per frame
    void Update()
    {
        handleInput();

        handleMovement();

    }
    void handleInput()
    {
        //keyboard
        if (Input.GetButtonDown("Horizontal"))
        {
            horiz = !horiz;
        }

        if (Input.GetButtonDown("Vertical"))
        {
            vert = !vert;
        }

        //oculus
        Vector2 touchPos = OVRInput.Get(OVRInput.Axis2D.PrimaryTouchpad, OVRInput.Controller.RTrackedRemote);


    }
    void handleMovement()
    {

        //keyboard
        if (horiz)
        {
            transform.Translate((Input.GetAxis("Horizontal") * 5) * Time.deltaTime, 0, 0);

        }
        if (vert)
        {
            transform.Translate(0, 0, (Input.GetAxis("Vertical") * 5) * Time.deltaTime);

        }

        // oculus remote

        Vector2 tPos = OVRInput.Get(OVRInput.Axis2D.PrimaryTouchpad, OVRInput.Controller.RTrackedRemote);

        float x = tPos.x;
        float z = tPos.y;

        transform.Translate(0, 0, z / 2);
        transform.Translate(x / 2, 0, 0);




    }
}


