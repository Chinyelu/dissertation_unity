﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class followScript : MonoBehaviour
{

    //movement target
    public Transform target;


    //or could have a vector3 object for the target and put in the vector 
    //public Vector3 target;

    // set an arbitraty speed
    public float speed = 5;

    //flag to set if we are moving or not
    bool isMoving = false;

    // Start is called before the first frame update
    void Start()
    {
        print("SUP BITCHES");
    }

    // Update is called once per frame
    // this is a good place to check stuff each frame
    // seemingly the main method
    void Update()
    {
        //check for input
        HandleInput();

        // move the player
        if (isMoving)
        {
            HandleMovement();
        }

        // always rotate towards the target
        transform.LookAt(target);
    }

    void HandleInput()
    {
        //check if input has been pressed (fire 1)
        //edit/project settings/input to find all inputs
        // fire1 is the left CTRL button or left click on mouse
        if (Input.GetButtonDown("Fire1"))
        {
            isMoving = !isMoving;
        }

        if (OVRInput.Get(OVRInput.Touch.PrimaryTouchpad))
        {
            isMoving = !isMoving;
        }

       
    }


    void HandleMovement()
    {

        if (!isMoving) return;

        //need to calc distance from the target each frame
        float distance = Vector3.Distance(transform.position, target.position);

        // check if arrived at the target
        if (distance > 0)
        {

            //calculate how much we need to move (step) d = v * t
            float step = speed * Time.deltaTime;

            // move by the step
            transform.position = Vector3.MoveTowards(transform.position, target.position, step);
        }


    }
}
